---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  message = FALSE
)
```

# zotero

<!-- badges: start -->
<!-- badges: end -->

Export of all my Zotero library data.

```{r zotero}
library(janitor)
library(readr)

(My_Library <- read_csv("My Library.csv") %>%
  clean_names())
```
