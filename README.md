
<!-- README.md is generated from README.Rmd. Please edit that file -->

# zotero

<!-- badges: start -->

<!-- badges: end -->

Export of all my Zotero library data.

``` r
library(janitor)
library(readr)

(My_Library <- read_csv("My Library.csv") %>%
  clean_names())
#> # A tibble: 152 x 87
#>    key   item_type publication_year author title publication_tit… isbn  issn 
#>    <chr> <chr>                <dbl> <chr>  <chr> <chr>            <chr> <chr>
#>  1 M8DS… book                  2008 Elkan… Lear… <NA>             <NA>  <NA> 
#>  2 42NG… journalA…               NA <NA>   Anal… <NA>             <NA>  <NA> 
#>  3 IRMA… journalA…               NA Niu, … Rece… <NA>             <NA>  <NA> 
#>  4 T28D… bookSect…             1998 Denis… PAC … Algorithmic Lea… 978-… <NA> 
#>  5 YYQ9… book                    NA He, J… Naiv… <NA>             <NA>  <NA> 
#>  6 3XLN… blogPost              2016 Perez… Game… Intuition Machi… <NA>  <NA> 
#>  7 N5RW… encyclop…             2018 <NA>   Cohe… Wikipedia        <NA>  <NA> 
#>  8 MZ2Z… blogPost              2015 Brown… 8 Ta… Machine Learnin… <NA>  <NA> 
#>  9 HQ7U… journalA…               NA Xu, Y… Mult… <NA>             <NA>  <NA> 
#> 10 ZI6C… conferen…             2006 Wei, … Semi… <NA>             978-… <NA> 
#> # … with 142 more rows, and 79 more variables: doi <chr>, url <chr>,
#> #   abstract_note <chr>, date <chr>, date_added <dttm>, date_modified <dttm>,
#> #   access_date <dttm>, pages <chr>, num_pages <dbl>, issue <dbl>,
#> #   volume <dbl>, number_of_volumes <lgl>, journal_abbreviation <chr>,
#> #   short_title <chr>, series <chr>, series_number <lgl>, series_text <lgl>,
#> #   series_title <chr>, publisher <chr>, place <chr>, language <chr>,
#> #   rights <chr>, type <chr>, archive <lgl>, archive_location <chr>,
#> #   library_catalog <chr>, call_number <lgl>, extra <chr>, notes <chr>,
#> #   file_attachments <chr>, link_attachments <chr>, manual_tags <chr>,
#> #   automatic_tags <chr>, editor <chr>, series_editor <chr>, translator <lgl>,
#> #   contributor <lgl>, attorney_agent <lgl>, book_author <lgl>,
#> #   cast_member <lgl>, commenter <lgl>, composer <lgl>, cosponsor <lgl>,
#> #   counsel <lgl>, interviewer <lgl>, producer <lgl>, recipient <lgl>,
#> #   reviewed_author <lgl>, scriptwriter <lgl>, words_by <lgl>, guest <lgl>,
#> #   number <chr>, edition <chr>, running_time <chr>, scale <lgl>, medium <lgl>,
#> #   artwork_size <lgl>, filing_date <lgl>, application_number <lgl>,
#> #   assignee <lgl>, issuing_authority <lgl>, country <lgl>, meeting_name <lgl>,
#> #   conference_name <chr>, court <lgl>, references <lgl>, reporter <lgl>,
#> #   legal_status <lgl>, priority_numbers <lgl>, programming_language <chr>,
#> #   version <chr>, system <lgl>, code <lgl>, code_number <lgl>, section <lgl>,
#> #   session <lgl>, committee <lgl>, history <lgl>, legislative_body <lgl>
```
